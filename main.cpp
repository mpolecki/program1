#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include <vector>
#include <filesystem>
#include <random>
#include "app_params.hpp"

int main(int argc, char **argv) 
{
    App_param params = parse_App_param(argc, argv);

    std:: cout << std::endl << std::endl << "Wynik:" << std::endl << std::endl;

    std::default_random_engine randEngine;
    std::uniform_int_distribution<int> intDistro(0,params.alphabet.size()-1);
    int random = 0;

    for (int row = 0; row < params.number_of_rows; row++)
    {
            for(int columns = 0; columns < params.number_of_columns; columns++)
            {
                //print sign
                random = intDistro(randEngine);
                std:: cout << params.alphabet[random];
            }

            std:: cout << std::endl;
    }

    std:: cout << std::endl;

    return 0;
}
