cmake_minimum_required(VERSION 3.0.0)
project(zadanie_parsowanie VERSION 0.1.0)

# Set C++ standard to 17
set(CMAKE_CXX_STANDARD 17)

# It cause to trait above line as requirement; if OFF some decay may happen to lower standard
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} " -static")

add_subdirectory(googletest)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

add_subdirectory(app_params)
add_executable(zadanie_parsowanie main.cpp )
target_link_libraries(zadanie_parsowanie PRIVATE app_params_library)


 



