# Zadanie parsowania

## Opis działania
1. Program odczytuje dane wprowadzone z terminala.
2. Użytkownik podaje liczbę wierszy, liczbę kolumn oraz alfabet. Przykład: **zadanie-parsowania.exe -d 3 -n 5 -a abc**
3. Program tworzy macierz o wymiarach d na n.
4. W każdym polu losowana jest wartość z alfabetu podanego w ostatnim parametrze
5. W przypadku braku wprowadzenie poprawnych parametrów ustawiane są domyślne parametry: **d=3, n=6, a=xyzabc**