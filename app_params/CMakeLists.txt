cmake_minimum_required(VERSION 3.0.0)

add_library(app_params_library STATIC app_params.cpp)
target_include_directories(app_params_library PUBLIC .)
