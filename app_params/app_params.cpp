
#include <iostream>
#include <vector>
#include <string>
#include "app_params.hpp"
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <optional>

static bool check_if_number( std::string s ) 
{
   for( int i = 0; i < s.length(); i++ ) 
   {
      if( !isdigit( s[i] )) 
      {
         return false;
      }
   }
   return true;
}

/*
struct Arg_result
{
    bool success = false;
    std::string value ="";
};
*/

std::optional<std::string> Find_argument_value(const std::vector<std::string> &input_params, std::string pattern, std::vector<int>& indexes_to_parse)
{
    //Arg_result results{};
    std::optional<std::string> result;
    std::vector<int> indexes_to_remove;

    //dla wszystkich wartosci indeksu sprawdzac
    for (auto& index : indexes_to_parse)
    {
        if ( (input_params[index] == pattern) && ( (index+1) < input_params.size() ) )
        {
            //zapisz znaleziony wynik
            //results.success = true;
            //results.value = input_params[index+1];
            result = input_params[index+1];;

            //zaznacz, ktore indeksy trzeba wywalic
            indexes_to_remove.push_back(index);
            indexes_to_remove.push_back(index+1);

            break;
        }
    }

    //wywal juz przeszukane indeksy
    for (int i = 0; i < indexes_to_remove.size(); ++i)
    {
        auto it = std::find(indexes_to_parse.begin(), indexes_to_parse.end(), indexes_to_remove[i]);
        indexes_to_parse.erase(it);
    }

    return result;
}

void save_arguments(std::vector<std::string>, int argc, char **argv)
{
    std::vector<std::string> str_vect;
    for(int i = 0; i < (argc); i++)
    {
        try
        {
            str_vect.push_back(argv[i]);
        }
        catch(const std::exception& e)
        {
            
        }
    }
}

void fill_index_list(std::vector<std::string> str_vect, std::vector<int> index_list)
{
    for (int i = 0; i < str_vect.size(); i++)
    {
        index_list.push_back(i);
    }
}

App_param parse_App_param(int argc, char **argv)
{
    App_param app_param{};
    std::vector<int> index_list;
    std::vector<std::string> str_vect;
    std::optional<std::string> result;

    //wczytac liste do wektora
    save_arguments(str_vect, argc, argv);

    //lista indeksow
    fill_index_list(str_vect, index_list);

    //sprawdzic czy mamy parametr -d

    result = Find_argument_value(str_vect, "-n", index_list);

    if ((result.has_value()) && (check_if_number(result.value())))
    {
        app_param.number_of_rows = stoi(result.value());
    }

    //sprawdzic czy mamy -n
    result = Find_argument_value(str_vect, "-d", index_list);
    if ((result.has_value()) && (check_if_number(result.value())))
    {
        app_param.number_of_columns = stoi(result.value());
    }

    //sprawdzcic czy mamy -a
    result = Find_argument_value(str_vect, "-a", index_list);
    if (result.has_value())
    {
        app_param.alphabet = result.value();
    }

    return app_param;
}