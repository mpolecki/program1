#ifndef APP_PARAMS_H
#define APP_PARAMS_H

#include <string>

struct App_param
{
    int number_of_rows = 1;
    int number_of_columns = 2;
    std::string alphabet = "p1b";
};

App_param parse_App_param(int argc, char **argv);

#endif